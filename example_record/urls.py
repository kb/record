# -*- encoding: utf-8 -*-
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import include, path, re_path

from .views import (
    DashView,
    ExampleDocumentCreateView,
    HomeView,
    RecordConversionDownloadView,
    RecordDownloadView,
    SettingsView,
)


urlpatterns = [
    re_path(r"^$", view=HomeView.as_view(), name="project.home"),
    re_path(r"^dash/$", view=DashView.as_view(), name="project.dash"),
    re_path(
        r"^settings/$",
        view=SettingsView.as_view(),
        name="project.settings",
    ),
    re_path(r"^", view=include("login.urls")),
    # download record conversion
    re_path(
        r"^example/record/(?P<pk>\d+)/(?P<file_type>[-\w]+)/download/$",
        view=RecordConversionDownloadView.as_view(),
        name="example.record.download.conversion",
    ),
    re_path(
        r"^example/record/(?P<pk>\d+)/(?P<file_type>[-\w]+)/(?P<alias>[-\w]+)/download/$",
        view=RecordConversionDownloadView.as_view(),
        name="example.record.download.conversion",
    ),
    # download record
    re_path(
        r"^example/record/(?P<pk>\d+)/download/$",
        view=RecordDownloadView.as_view(),
        name="example.record.download",
    ),
    re_path(r"^record/", view=include("record.urls")),
    re_path(
        r"^upload/$",
        view=ExampleDocumentCreateView.as_view(),
        name="example.document.create",
    ),
]

urlpatterns += staticfiles_urlpatterns()

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        re_path(r"^__debug__/", include(debug_toolbar.urls))
    ] + urlpatterns
