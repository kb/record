# -*- encoding: utf-8 -*-
import os
import pytest

from datetime import date
from django.conf import settings
from django.utils import timezone
from freezegun import freeze_time
from pathlib import Path

from example_record.tests.factories import ExampleDocumentFactory
from login.tests.factories import UserFactory
from record.models import Conversion, Record, RecordError
from record.tests.factories import ConversionFactory, RecordFactory


@pytest.mark.django_db
def test_complete_conversions():
    record = RecordFactory(content_object=ExampleDocumentFactory(), record=None)
    complete = timezone.now()
    ConversionFactory(record=record, file_type="jpg", status="a")
    ConversionFactory(
        record=record,
        file_type="pdf",
        status="b",
        completed_date=timezone.now(),
    )
    assert ["b"] == [x.status for x in record.complete_conversions()]


@pytest.mark.django_db
def test_conversion_types():
    record = RecordFactory(content_object=ExampleDocumentFactory(), record=None)
    ConversionFactory(
        record=record, completed_date=timezone.now(), file_type="jpg"
    )
    ConversionFactory(
        record=record, completed_date=timezone.now(), file_type="pdf"
    )
    assert ["jpg", "pdf"]


@pytest.mark.django_db
def test_conversions():
    document = ExampleDocumentFactory()
    record = Conversion.objects.create_conversions(
        UserFactory(), document, Record.FILE_TYPES
    )
    assert ["jpg", "pdf"] == [x.file_type for x in record.conversions()]


@pytest.mark.django_db
def test_create_conversions():
    document = ExampleDocumentFactory()
    record = RecordFactory(content_object=document, record=None)
    record.create_conversions()
    conversions = Conversion.objects.filter(record=record).order_by("file_type")
    assert ["jpg", "pdf"] == [x.file_type for x in conversions]


@pytest.mark.django_db
def test_create_conversions_pdf():
    document = ExampleDocumentFactory()
    record = RecordFactory(content_object=document, record=None)
    record.create_conversions([Record.PDF])
    conversions = Conversion.objects.filter(record=record).order_by("file_type")
    assert ["pdf"] == [x.file_type for x in conversions]


@pytest.mark.django_db
def test_create_record():
    content_object = ExampleDocumentFactory()
    user = UserFactory()
    record = Record.objects.create_record(
        "agenda", user, content_object=content_object
    )
    assert content_object == record.content_object
    assert user == record.user
    assert record.is_file is False


@pytest.mark.django_db
def test_create_record_file_name():
    file_name = Path(
        settings.BASE_DIR, settings.MEDIA_ROOT, "data", "1-2-3.doc"
    )
    user = UserFactory()
    with freeze_time(date(2018, 3, 11)):
        record = Record.objects.create_record(
            "testing", user, file_name=file_name
        )
    assert record.content_object is None
    # The folder is called 2018 because of 'freeze_time' (see above)
    assert "/testing/2018/03/" in record.record.path, record.record.path
    assert "1-2-3.doc" == record.original_file_name
    assert record.is_file is True
    assert record.record.path == record.original_file_path()
    assert user == record.user


@pytest.mark.django_db
def test_create_record_from_bytes():
    data = b"Carrots\n"
    user = UserFactory()
    record = Record.objects.create_record_from_bytes(
        "cake", user, data, "carrot.txt"
    )
    record.refresh_from_db()
    assert user == record.user
    assert "cake" == record.folder_name
    assert "carrot.txt" == record.original_file_name
    data = record.record.read()
    assert b"Carrots\n" == data


@pytest.mark.django_db
def test_create_record_no_content_object():
    content_object = ExampleDocumentFactory()
    with pytest.raises(RecordError) as e:
        Record.objects.create_record("agenda", UserFactory())
    assert (
        "A record must be created with a 'file_name' or 'content_object'"
        in str(e.value)
    )


@pytest.mark.django_db
def test_file_size_in_bytes():
    file_name = Path(
        settings.BASE_DIR, settings.MEDIA_ROOT, "data", "1-2-3.doc"
    )
    user = UserFactory()
    record = Record.objects.create_record(
        "testing", UserFactory(), file_name=file_name
    )
    assert 9216 == record.file_size_in_bytes()


@pytest.mark.django_db
def test_for_content_object():
    document = ExampleDocumentFactory()
    record = RecordFactory(content_object=document, record=None)
    assert record == Record.objects.for_content_object(document)


@pytest.mark.django_db
def test_original_file_path():
    file_name = Path(
        settings.BASE_DIR, settings.MEDIA_ROOT, "data", "1-2-3.doc"
    )
    record = Record.objects.create_record(
        "testing", UserFactory(), file_name=file_name
    )
    assert "{}-1-2-3.doc".format(record.pk) in record.original_file_path()


@pytest.mark.django_db
def test_original_file_path():
    record = RecordFactory(record=None)
    with pytest.raises(RecordError) as e:
        record.original_file_path()
    assert "Record '{}' has no record or 'content_object'".format(
        record.pk
    ) in str(e.value)
