# -*- encoding: utf-8 -*-
import pytest

from django.conf import settings
from pathlib import Path

from login.tests.factories import UserFactory
from example_record.models import ExampleRecord
from record.models import Record


@pytest.mark.django_db
def test_create_example_record():
    user = UserFactory(username="patrick")
    for name in ("1-2-3.doc", "4-5-6.doc", "7-8-9.doc"):
        file_name = Path(settings.BASE_DIR, settings.MEDIA_ROOT, "data", name)
        ExampleRecord.objects.create_example_record(user, file_name)
    assert set(["1-2-3.doc", "4-5-6.doc", "7-8-9.doc"]) == set(
        [x.original_file_name for x in Record.objects.all()]
    )
