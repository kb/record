# -*- encoding: utf-8 -*-
import pytest

from unittest import mock

from example_record.tests.factories import ExampleDocumentFactory
from login.tests.factories import UserFactory
from record.models import Conversion, Record, RecordError
from record.service import convert_records, convert_to


@pytest.mark.django_db
def test_convert_records():
    with mock.patch("record.service.convert_to"):
        document = ExampleDocumentFactory()
        user = UserFactory()
        Conversion.objects.create_conversions(user, document, [Record.PDF])
        # will not convert any records because we have mocked the conversion
        result = convert_records()
        assert 0 == result
        assert 1 == Conversion.objects.count()
        conversion = Conversion.objects.first()
        assert conversion.completed_date is None
        assert 1 == conversion.retries


@pytest.mark.django_db
def test_convert_to():
    with pytest.raises(RecordError) as e:
        convert_to("abc", "", "")
    assert "Invalid file type" in str(e.value)
    assert "Should be one of ['jpg', 'pdf']" in str(e.value)
