# -*- encoding: utf-8 -*-
import io
import pytest

from django.conf import settings
from django.urls import reverse
from http import HTTPStatus
from pathlib import Path

from example_record.tests.factories import ExampleDocumentFactory
from login.tests.factories import TEST_PASSWORD, UserFactory
from record.models import Record
from record.tests.factories import ConversionFactory, RecordFactory


@pytest.mark.django_db
def test_record_download(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    file_name = Path(
        settings.BASE_DIR, settings.MEDIA_ROOT, "data", "1-2-3.txt"
    )
    record = Record.objects.create_record("testing", user, file_name=file_name)
    response = client.get(reverse("example.record.download", args=[record.pk]))
    assert HTTPStatus.OK == response.status_code
    content = io.BytesIO(b"".join(response.streaming_content))
    assert "1-2-3" == content.read().decode("utf-8").strip()
    assert (
        'inline; filename="{}-1-2-3'.format(record.pk)
        in response["Content-Disposition"]
    )
    assert '.txt"'.format(record.pk) in response["Content-Disposition"]


@pytest.mark.django_db
def test_record_download_conversion(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    document = ExampleDocumentFactory(document__data=b"abc")
    record = RecordFactory(content_object=document, record=None)
    ConversionFactory(
        record=record,
        file_type=Record.PDF,
        converted_file__data=b"converted-data",
    )
    response = client.get(
        reverse(
            "example.record.download.conversion", args=[document.pk, Record.PDF]
        )
    )
    assert HTTPStatus.OK == response.status_code
    content = io.BytesIO(b"".join(response.streaming_content))
    assert "converted-data" == content.read().decode("utf-8").strip()
    assert 'inline; filename="example_' in response["Content-Disposition"]
