# -*- encoding: utf-8 -*-
import pytest

from django.db import IntegrityError
from django.utils import timezone
from django.urls import reverse

from example_record.tests.factories import ExampleDocumentFactory
from login.tests.factories import UserFactory
from record.models import Conversion, Record
from record.tests.factories import ConversionFactory, RecordFactory


@pytest.mark.django_db
def test_create_conversions():
    document = ExampleDocumentFactory()
    user = UserFactory()
    record = Conversion.objects.create_conversions(
        user, document, Record.FILE_TYPES
    )
    assert document == record.content_object
    conversions = Conversion.objects.filter(record=record).order_by("file_type")
    assert ["jpg", "pdf"] == [x.file_type for x in conversions]


@pytest.mark.django_db
def test_get_file_path():
    record = RecordFactory(content_object=ExampleDocumentFactory(), record=None)
    x = ConversionFactory(record=record, file_type="jpg")
    assert "media-private/record/converted" in x.get_file_path()


@pytest.mark.django_db
def test_is_image():
    record = RecordFactory(content_object=ExampleDocumentFactory(), record=None)
    obj = ConversionFactory(record=record, file_type="jpg")
    assert obj.is_image is True


@pytest.mark.django_db
def test_is_image_not():
    record = RecordFactory(content_object=ExampleDocumentFactory(), record=None)
    obj = ConversionFactory(record=record, file_type="pdf")
    assert obj.is_image is False


@pytest.mark.django_db
def test_unique_together():
    record = RecordFactory(content_object=ExampleDocumentFactory(), record=None)
    ConversionFactory(record=record, file_type="jpg")
    with pytest.raises(IntegrityError) as e:
        ConversionFactory(record=record, file_type="jpg")


@pytest.mark.django_db
def test_not_converted():
    record = RecordFactory(content_object=ExampleDocumentFactory(), record=None)
    ConversionFactory(record=record, file_type="jpg", status="a")
    ConversionFactory(
        record=record,
        file_type="pdf",
        status="b",
        completed_date=timezone.now(),
    )
    assert ["a"] == [x.status for x in Conversion.objects.not_converted()]


@pytest.mark.django_db
def test_not_converted_record_pk():
    record = RecordFactory(content_object=ExampleDocumentFactory(), record=None)
    ConversionFactory(record=record, file_type="jpg", status="a")
    ConversionFactory(
        record=RecordFactory(
            content_object=ExampleDocumentFactory(), record=None
        ),
        file_type="pdf",
        status="b",
    )
    assert ["a"] == [
        x.status for x in Conversion.objects.not_converted(record.pk)
    ]


@pytest.mark.django_db
def test_not_converted_max_retry():
    record = RecordFactory(content_object=ExampleDocumentFactory(), record=None)
    ConversionFactory(record=record, file_type="jpg", status="a")
    ConversionFactory(record=record, file_type="pdf", status="b", retries=99)
    assert ["a"] == [x.status for x in Conversion.objects.not_converted()]
