# -*- encoding: utf-8 -*-
import pytest

from django.conf import settings
from django.urls import reverse
from pathlib import Path

from example_record.tests.factories import ExampleDocumentFactory
from login.tests.factories import UserFactory
from login.tests.fixture import perm_check
from record.models import Conversion, Record, RecordError
from record.tests.factories import ConversionFactory, RecordFactory


@pytest.mark.django_db
def test_conversion_detail(perm_check):
    document = ExampleDocumentFactory(document__data=b"def")
    record = RecordFactory(content_object=document, record=None)
    conversion = ConversionFactory(
        record=record, file_type=Record.PDF, converted_file__data=b"def"
    )
    perm_check.admin(reverse("record.conversion", args=[conversion.pk]))


@pytest.mark.django_db
def test_list(perm_check):
    document = ExampleDocumentFactory()
    Conversion.objects.create_conversions(
        UserFactory(), document, Record.FILE_TYPES
    )
    perm_check.staff(reverse("record.list"))


@pytest.mark.django_db
def test_view_record_download(perm_check):
    file_name = Path(
        settings.BASE_DIR, settings.MEDIA_ROOT, "data", "1-2-3.txt"
    )
    record = Record.objects.create_record(
        "testing", UserFactory(), file_name=file_name
    )
    perm_check.staff(reverse("example.record.download", args=[record.pk]))


@pytest.mark.django_db
def test_view_record_download_conversion_pdf(perm_check):
    document = ExampleDocumentFactory(document__data=b"abc")
    record = RecordFactory(content_object=document, record=None)
    ConversionFactory(
        record=record, file_type=Record.PDF, converted_file__data=b"def"
    )
    perm_check.staff(
        reverse(
            "example.record.download.conversion", args=[document.pk, Record.PDF]
        )
    )


@pytest.mark.django_db
def test_view_upload(perm_check):
    perm_check.staff(reverse("example.document.create"))
