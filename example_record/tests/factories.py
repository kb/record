# -*- encoding: utf-8 -*-
import factory

from example_record.models import ExampleDocument


class ExampleDocumentFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ExampleDocument

    document = factory.django.FileField()
