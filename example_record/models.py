# -*- encoding: utf-8 -*-
import os

from django.conf import settings
from django.db import models, transaction

from record.models import Record


class ExampleDocument(models.Model):
    document = models.FileField(upload_to="testing/document")

    class Meta:
        ordering = ("pk",)
        verbose_name = "Example Document"
        verbose_name_plural = "Example Documents"

    def __str__(self):
        return f"{self.document.file.name}"

    def original_file_path(self):
        return os.path.join(
            self.document.field.storage.base_location, self.document.file.name
        )


class ExampleRecordManager(models.Manager):
    def create_example_record(self, user, file_name):
        with transaction.atomic():
            record = Record.objects.create_record(
                "testing", user, file_name=file_name
            )
            x = ExampleRecord(record=record, user=user)
            x.save()
        return x


class ExampleRecord(models.Model):
    record = models.OneToOneField(Record, on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    objects = ExampleRecordManager()

    class Meta:
        ordering = ("pk",)
        verbose_name = "Example Record"
        verbose_name_plural = "Example Records"

    def __str__(self):
        return (
            f"{self.record.record.original_file_name} "
            f"(for {self.user.username})"
        )
