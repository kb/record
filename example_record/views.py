# -*- encoding: utf-8 -*-
from braces.views import LoginRequiredMixin, StaffuserRequiredMixin
from django.db import transaction
from django.urls import reverse
from django.views.generic import CreateView, TemplateView, View
from django_sendfile import sendfile

from base.view_utils import BaseMixin
from record.models import Conversion, Record
from record.views import (
    Conversion,
    RecordConversionDownloadMixin,
    RecordDownloadMixin,
)
from .forms import ExampleDocumentForm
from .models import ExampleDocument


class ExampleDocumentCreateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, CreateView
):
    model = ExampleDocument
    form_class = ExampleDocumentForm
    template_name = "example/exampledocument_form.html"

    def form_valid(self, form):
        with transaction.atomic():
            result = super().form_valid(form)
            Conversion.objects.create_conversions(
                self.object, Record.FILE_TYPES
            )
        return result

    def get_success_url(self):
        return reverse("record.list")


class HomeView(BaseMixin, TemplateView):
    template_name = "example/home.html"


class DashView(LoginRequiredMixin, BaseMixin, TemplateView):
    template_name = "example/dash.html"


class RecordConversionDownloadView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    RecordConversionDownloadMixin,
    BaseMixin,
    View,
):
    def get(self, request, *args, **kwargs):
        pk = kwargs.get("pk")
        document = ExampleDocument.objects.get(pk=pk)
        file_path = self.get_record(document, **kwargs)
        return sendfile(self.request, file_path)


class RecordDownloadView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    RecordDownloadMixin,
    BaseMixin,
    View,
):
    def get(self, request, *args, **kwargs):
        pk = kwargs.get("pk")
        record = Record.objects.get(pk=pk)
        file_path = self.get_record(record)
        return sendfile(self.request, file_path)


class SettingsView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, TemplateView
):
    template_name = "example/settings.html"
