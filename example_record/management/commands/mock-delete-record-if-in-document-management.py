# -*- encoding: utf-8 -*-
import requests

from django.contrib.auth import get_user_model
from django.core.files import File
from django.core.management.base import BaseCommand

from login.models import SYSTEM_GENERATED
from record.models import Record


class Command(BaseCommand):
    help = (
        "Create a 'Record' to test the "
        "'delete-record-if-in-document-management'"
        "management command. #6328"
    )

    def handle(self, *args, **options):
        self.stdout.write(f"{self.help}")
        r = requests.get("https://www.kbsoftware.co.uk/static/web/img/logo.png")
        with open("/tmp/kb-logo.png", "wb") as f:
            f.write(r.content)
        system_generated_user = get_user_model().objects.get(
            username=SYSTEM_GENERATED
        )
        record = Record.objects.create_record(
            "testing-record",
            system_generated_user,
            file_name="/tmp/kb-logo.png",
        )
        record.document_management_id = 9999
        record.save()
        self.stdout.write(f"Created {record}")
        self.stdout.write(f"{self.help} - Complete")
