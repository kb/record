# -*- encoding: utf-8 -*-
import os

from django.core.files import File
from django.core.management.base import BaseCommand

from example_record.models import ExampleDocument
from record.models import Conversion, Record
from record.service import convert_records


class Command(BaseCommand):
    help = "Convert document to PDF"

    def add_arguments(self, parser):
        parser.add_argument("file_name", type=str)

    def handle(self, *args, **options):
        self.stdout.write(self.help)
        file_name = options["file_name"]
        with open(file_name, "rb") as f:
            django_file = File(f)
            example = ExampleDocument()
            example.document.save(
                os.path.basename(file_name), django_file, save=True
            )
        record = Conversion.objects.create_conversions(
            example, Record.FILE_TYPES
        )
        if convert_records(record.pk):
            qs = record.conversion_set.all()
            if qs.count():
                self.stdout.write("Created {} files".format(qs.count()))
                for conversion in qs:
                    self.stdout.write(str(conversion.converted_file.path))
            self.stdout.write("{} - Complete".format(self.help))
        else:
            self.stdout.write("{} - Failed to convert file".format(self.help))
