# Generated by Django 3.0.5 on 2020-04-13 09:21

from django.conf import settings
import django.core.files.storage
from django.db import migrations, models
import django.db.models.deletion
import record.models


class Migration(migrations.Migration):
    dependencies = [
        ("contenttypes", "0002_remove_content_type_name"),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("record", "0001_initial"),
    ]

    operations = [
        migrations.RenameField(
            model_name="conversion",
            old_name="complete",
            new_name="completed_date",
        ),
        migrations.RemoveField(
            model_name="conversion",
            name="retry_count",
        ),
        migrations.AddField(
            model_name="conversion",
            name="fail_date",
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name="conversion",
            name="retries",
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name="record",
            name="document_management_id",
            field=models.IntegerField(
                blank=True,
                help_text="ID of the record in a Document Management System",
                null=True,
            ),
        ),
        migrations.AddField(
            model_name="record",
            name="folder_name",
            field=models.SlugField(
                default="default",
                help_text="Name of the folder (may be used by the file upload)",
            ),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name="record",
            name="original_file_name",
            field=models.CharField(blank=True, max_length=255),
        ),
        migrations.AddField(
            model_name="record",
            name="record",
            field=models.FileField(
                blank=True,
                storage=django.core.files.storage.FileSystemStorage(
                    location="media-private"
                ),
                upload_to=record.models.upload_to_record,
            ),
        ),
        migrations.AddField(
            model_name="record",
            name="user",
            field=models.ForeignKey(
                default=1,
                on_delete=django.db.models.deletion.CASCADE,
                to=settings.AUTH_USER_MODEL,
            ),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name="record",
            name="content_type",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                to="contenttypes.ContentType",
            ),
        ),
        migrations.AlterField(
            model_name="record",
            name="object_id",
            field=models.PositiveIntegerField(blank=True, null=True),
        ),
    ]
