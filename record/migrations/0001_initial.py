# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-10 15:44
from __future__ import unicode_literals

import django.core.files.storage
from django.db import migrations, models
import django.db.models.deletion
import easy_thumbnails.fields


class Migration(migrations.Migration):
    initial = True

    dependencies = [("contenttypes", "0002_remove_content_type_name")]

    operations = [
        migrations.CreateModel(
            name="Conversion",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("created", models.DateTimeField(auto_now_add=True)),
                ("modified", models.DateTimeField(auto_now=True)),
                (
                    "file_type",
                    models.CharField(
                        choices=[("jpg", "JPEG"), ("pdf", "Adobe Acrobat")],
                        max_length=3,
                    ),
                ),
                (
                    "converted_file",
                    easy_thumbnails.fields.ThumbnailerField(
                        storage=django.core.files.storage.FileSystemStorage(
                            location="media-private"
                        ),
                        upload_to="record/converted",
                    ),
                ),
                ("retry_count", models.IntegerField(default=0)),
                ("complete", models.DateTimeField(blank=True, null=True)),
                ("status", models.TextField(blank=True)),
            ],
            options={
                "verbose_name_plural": "Conversions",
                "verbose_name": "Conversion",
            },
        ),
        migrations.CreateModel(
            name="Record",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("created", models.DateTimeField(auto_now_add=True)),
                ("modified", models.DateTimeField(auto_now=True)),
                ("object_id", models.PositiveIntegerField()),
                (
                    "content_type",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to="contenttypes.ContentType",
                    ),
                ),
            ],
            options={
                "ordering": ["-created"],
                "verbose_name_plural": "Records",
                "verbose_name": "Record",
            },
        ),
        migrations.AddField(
            model_name="conversion",
            name="record",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE, to="record.Record"
            ),
        ),
        migrations.AlterUniqueTogether(
            name="conversion", unique_together=set([("record", "file_type")])
        ),
    ]
