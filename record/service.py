# -*- encoding: utf-8 -*-
import logging
import os
import subprocess

from django.utils import timezone
from easy_thumbnails.files import generate_all_aliases

from .models import Conversion, Record, RecordError


logger = logging.getLogger(__name__)


def _new_file_name(original_file_path, file_type):
    "Create a new file name from the original path." ""
    base_name = os.path.basename(original_file_path)
    no_extension = os.path.splitext(base_name)[0]
    return "{}.{}".format(no_extension, file_type)


def convert_records(record_pk=None):
    """Convert pending records.

    Keyword arguments:
    record_pk -- primary key of the record to check (default None)

    """
    count = 0
    qs = Conversion.objects.not_converted(record_pk)
    message = " ('record_pk' {})".format(record_pk) if record_pk else ""
    logger.info("convert_records: count {}{}".format(qs.count(), message))
    for conversion in qs:
        file_path = None
        out_lines = []
        original_file_path = conversion.record.original_file_path()
        logger.info("original_file_path: '{}'".format(original_file_path))
        if original_file_path:
            out_directory = os.path.join(
                conversion.converted_file.field.storage.base_location,
                conversion.converted_file.field.upload_to,
            )
            # convert the original file
            out_lines = convert_to(
                conversion.file_type, original_file_path, out_directory
            )
            # can we find the new file?
            file_name = os.path.join(
                conversion.converted_file.field.upload_to,
                _new_file_name(original_file_path, conversion.file_type),
            )
            file_path = os.path.join(
                conversion.converted_file.field.storage.base_location, file_name
            )
        complete = timezone.now()
        found = False
        if original_file_path and os.path.isfile(file_path):
            count += 1
            found = True
            conversion.converted_file = file_name
            conversion.save()
            if found and conversion.is_image:
                generate_all_aliases(
                    conversion.converted_file, include_global=True
                )
            conversion.complete = complete
            status = "Completed at {}.".format(
                complete.strftime("%d/%m/%Y %H:%M")
            )
        else:
            conversion.retries = conversion.retries + 1
            status = (
                "Cannot find converted file '{}' at {}. "
                "Tried {} times.".format(
                    file_path or str(conversion.pk),
                    complete.strftime("%d/%m/%Y %H:%M"),
                    conversion.retries,
                )
            )
            logger.error(status)
        conversion.status = "{}\n{}".format(status, "\n".join(out_lines))
        conversion.save()
    return count


def convert_to(file_type, file_name, out_directory):
    """Convert a file to PDF or JPG format.

    .. tip:: If the conversion isn't working, then it might be because
             LibreOffice is running on your Desktop.  Shut it down and the
             conversion should succeed.  The error message will probably be:
             *Cannot find converted file*.

    """
    if not file_type in Record.FILE_TYPES:
        raise RecordError(
            "Invalid file type '{}'.  Should be one "
            "of {}".format(file_type, Record.FILE_TYPES)
        )
    command = [
        "soffice",
        "--convert-to",
        file_type,
        file_name,
        "--headless",
        "--outdir",
        out_directory,
    ]
    # see 'tip' above if the conversion is failing
    proc = subprocess.Popen(command, stdout=subprocess.PIPE)
    result = []
    while True:
        line = proc.stdout.readline()
        if not line:
            break
        out_line = line.decode("utf-8")[:-1]
        logger.info("out: {}".format(out_line))
        if out_line.strip():
            result.append(out_line)
    return result
