# -*- encoding: utf-8 -*-
"""

The entry point for this  module is ``create_conversions`` which creates a
``Record`` in the database.  The ``Record`` has a ``content_object`` which must
have an ``original_file_path`` method.  This is used to retrieve the file to
convert.


"""
import logging
import os

from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.core.files import File
from django.core.files.base import ContentFile
from django.db import models
from django.urls import reverse
from easy_thumbnails.fields import ThumbnailerField
from pathlib import Path

from base.model_utils import (
    private_file_store,
    RetryModel,
    RetryModelManager,
    TimeStampedModel,
)


logger = logging.getLogger(__name__)


class RecordError(Exception):
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value

    def __str__(self):
        return repr("{}, {}".format(self.__class__.__name__, self.value))


# def upload_to_conversion(instance, filename):
#    path = Path(filename)
#    return "{}/{}/converted/{}-{}".format(
#        instance.record.folder_name,
#        instance.record.created.strftime("%Y/%m"),
#        instance.record.pk,
#        path.name,
#    )


def upload_to_record(instance, filename):
    path = Path(filename)
    created = instance.created.strftime("%Y/%m")
    return f"{instance.folder_name}/{created}/{instance.pk}-{path.name}"


class RecordManager(models.Manager):
    def create_record(
        self, folder_name, user, content_object=None, file_name=None
    ):
        if content_object:
            x = Record(
                user=user,
                folder_name=folder_name,
                content_object=content_object,
            )
            x.save()
        elif file_name:
            with open(file_name, "rb") as f:
                django_file = File(f)
                path = Path(file_name)
                x = Record(
                    user=user,
                    folder_name=folder_name,
                    original_file_name=path.name,
                )
                x.save()
                x.record.save(file_name, django_file, save=True)
        else:
            raise RecordError(
                "A record must be created with a "
                "'file_name' or 'content_object'"
            )
        return x

    def create_record_from_bytes(self, folder_name, user, data, file_name):
        """Create a record from data (in bytes).

        Copied from ```block.models.create_document_from_bytes```.

        """
        x = Record(
            user=user,
            folder_name=folder_name,
            original_file_name=file_name,
        )
        x.save()
        x.record.save(file_name, ContentFile(data), save=True)
        return x

    def create_record_from_request_files(
        self, folder_name, user, file_to_save, file_name
    ):
        """Create a record from the file in ``request.FILES``.

        e.g::

          file_to_save = request.FILES["file"]
          record = Record.objects.create_record_from_request_files(
              "pk-testing",
              self.request.user,
              file_to_save,
              file_to_save.name,
          )

        Copied from:
        https://stackoverflow.com/questions/505868/django-how-do-you-turn-an-inmemoryuploadedfile-into-an-imagefields-fieldfile

        """
        x = Record(
            user=user,
            folder_name=folder_name,
            original_file_name=file_name,
        )
        x.save()
        x.record.save(file_name, file_to_save, save=True)
        return x

    def for_content_object(self, obj):
        """Return the ``Record`` instance for ``obj``."""
        return self.model.objects.get(
            content_type=ContentType.objects.get_for_model(obj),
            object_id=obj.pk,
        )


class Record(TimeStampedModel):
    JPG = "jpg"
    PDF = "pdf"

    FILE_TYPES = [JPG, PDF]

    record = models.FileField(
        blank=True, storage=private_file_store, upload_to=upload_to_record
    )
    folder_name = models.SlugField(
        help_text="Name of the folder (may be used by the file upload)"
    )
    original_file_name = models.CharField(max_length=255, blank=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    document_management_id = models.IntegerField(
        help_text="ID of the record in a Document Management System",
        blank=True,
        null=True,
    )
    # link to the object in the system which requested the conversion
    content_type = models.ForeignKey(
        ContentType, blank=True, null=True, on_delete=models.CASCADE
    )
    object_id = models.PositiveIntegerField(blank=True, null=True)
    content_object = GenericForeignKey()
    objects = RecordManager()

    class Meta:
        ordering = ["-created"]
        verbose_name = "Record"
        verbose_name_plural = "Records"

    def __str__(self):
        return f"{self.pk}: '{self.original_file_name}'"

    def complete_conversions(self):
        return self.conversions().filter(completed_date__isnull=False)

    def conversion_types(self):
        result = []
        for conversion in self.complete_conversions():
            result.append(conversion.file_type)
        return result

    def conversions(self):
        return self.conversion_set.order_by("file_type")

    def create_conversions(self, file_types=None):
        if file_types is None:
            file_types = Record.FILE_TYPES
        for file_type in file_types:
            if file_type not in Record.FILE_TYPES:
                raise RecordError(
                    "'{}' is an invalid file type ('{}') for "
                    "a conversion record.".format(file_type)
                )
            conversion = Conversion(
                record=self,
                file_type=file_type,
                max_retry_count=Conversion.DEFAULT_MAX_RETRY_COUNT,
            )
            conversion.save()

    def file_size_in_bytes(self):
        result = None
        file_name = Path(self.record.path)
        if file_name.exists():
            result = file_name.stat().st_size
        return result

    @property
    def is_file(self):
        return bool(self.record)

    def original_file_path(self):
        result = None
        if self.is_file:
            result = self.record.path
        elif self.content_object:
            result = self.content_object.original_file_path()
        else:
            raise RecordError(
                "Record '{}' has no record or 'content_object'".format(self.pk)
            )
        return result


class ConversionManager(RetryModelManager):
    def create_conversions(self, user, content_object, file_types=None):
        record = Record.objects.create_record(
            "conversion", user, content_object=content_object
        )
        record.create_conversions(file_types)
        return record

    def current(self):
        return self.model.objects.all()

    def not_converted(self, record_pk=None):
        """Find all records pending PDF or JPG conversion."""
        qs = self.outstanding()
        if record_pk is not None:
            qs = qs.filter(record__pk=record_pk)
        return qs


class Conversion(RetryModel):
    CHOICES = [(Record.JPG, "JPEG"), (Record.PDF, "Adobe Acrobat")]
    DEFAULT_MAX_RETRY_COUNT = 5

    record = models.ForeignKey(Record, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    file_type = models.CharField(max_length=3, choices=CHOICES)
    converted_file = ThumbnailerField(
        upload_to="record/converted",
        storage=private_file_store,
        thumbnail_storage=private_file_store,
    )
    status = models.TextField(blank=True)
    objects = ConversionManager()

    class Meta:
        unique_together = ("record", "file_type")
        verbose_name = "Conversion"
        verbose_name_plural = "Conversions"

    def __str__(self):
        return "{}".format(self.pk)

    def get_file_path(self, alias=None):
        field = self.converted_file
        if alias and self.is_image:
            thumbnail_field = field[alias]
            file_path = os.path.join(
                thumbnail_field.storage.base_location, thumbnail_field.file.name
            )
        else:
            file_path = os.path.join(
                self.converted_file.field.storage.base_location,
                self.converted_file.file.name,
            )
        return file_path

    @property
    def is_image(self):
        return self.file_type == Record.JPG
