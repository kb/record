# -*- encoding: utf-8 -*-
import factory

from login.tests.factories import UserFactory
from record.models import Conversion, Record


class RecordFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Record

    record = factory.django.FileField()
    user = factory.SubFactory(UserFactory)


class ConversionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Conversion

    converted_file = factory.django.FileField()
    max_retry_count = Conversion.DEFAULT_MAX_RETRY_COUNT
