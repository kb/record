# -*- encoding: utf-8 -*-
import io
import pytest

from django.conf import settings
from django.urls import reverse
from http import HTTPStatus
from pathlib import Path

from login.tests.factories import TEST_PASSWORD, UserFactory
from record.models import Record
from .factories import RecordFactory


@pytest.mark.django_db
def test_views_record_detail(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    file_name = Path(
        settings.BASE_DIR, settings.MEDIA_ROOT, "data", "1-2-3.txt"
    )
    record = Record.objects.create_record("testing", user, file_name=file_name)
    response = client.get(reverse("record.view", args=[record.pk]))
    assert HTTPStatus.OK == response.status_code
    content = io.BytesIO(b"".join(response.streaming_content))
    assert "1-2-3" == content.read().decode("utf-8").strip()
    assert (
        'inline; filename="{}-1-2-3.txt"'.format(record.pk)
        == response["Content-Disposition"]
    )
