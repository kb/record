# -*- encoding: utf-8 -*-
import pytest

from record.service import _new_file_name


@pytest.mark.django_db
def test_new_file_name():
    original_file_path = "/home/patrick/version/Pressure.docx"
    assert "Pressure.pdf" == _new_file_name(original_file_path, "pdf")
