# -*- encoding: utf-8 -*-
import csv
import pathlib
import shutil

from dateutil.relativedelta import relativedelta
from django.core.management.base import BaseCommand, CommandError
from django.utils import timezone


from record.models import Record


class Command(BaseCommand):
    """Delete 'record' (just the file) if in document management.

    Will ignore:

    - anything created in the last three months.

    Documentation
    ``~/dev/project/navigator/docs-kb/source/management-commands.rst``

    """

    help = (
        "Delete record (just the file) if in document management "
        "and created over three months ago. "
        "The file will be moved to the 'temporary_folder' #6328"
    )

    def add_arguments(self, parser):
        parser.add_argument("folder_name", type=str)
        parser.add_argument("temporary_folder", type=str)

    def handle(self, *args, **options):
        self.stdout.write(f"{self.help}")
        date_as_string = timezone.now().strftime("%Y-%m-%d-%H%M")
        folder_name = options["folder_name"]
        csv_file_name = f"{date_as_string}-delete-{folder_name}-record-if-in-document-management.csv"
        temporary_folder = pathlib.Path(options["temporary_folder"])
        with open(csv_file_name, "w", newline="", encoding="utf-8") as out:
            csv_writer = csv.writer(out, dialect="excel-tab")
            filter_date = timezone.now() + relativedelta(
                months=-3, hour=23, minute=59, second=59, microsecond=999999
            )
            # for testing with 'mock-delete-record-if-in-document-management'
            filter_date = timezone.now()
            self.stdout.write(f"Find records before {filter_date}")
            qs = Record.objects.filter(
                created__lt=filter_date,
                document_management_id__isnull=False,
                folder_name=folder_name,
            ).order_by("id")
            self.stdout.write(
                f"Found {qs.count()} matching records before {filter_date}..."
            )
            count = 0
            for record in qs:
                delete_the_file = False
                try:
                    if pathlib.Path(record.record.path).exists():
                        delete_the_file = True
                except ValueError as e:
                    if (
                        "'record' attribute has no file associated with it"
                        in str(e)
                    ):
                        pass
                    else:
                        raise CommandError("Something went wrong", e)
                if delete_the_file:
                    # move file to the temporary_folder
                    move_file = pathlib.Path(
                        temporary_folder, record.record.name
                    )
                    move_file.parent.mkdir(parents=True, exist_ok=True)
                    self.stdout.write(
                        f"{record.pk}  "
                        f"{record.created}  "
                        # f"{record.folder_name} "
                        f"{record.document_management_id}  "
                        f"{record.record.name}"
                        f"{move_file}"
                    )
                    csv_writer.writerow(
                        [
                            record.pk,
                            record.created.strftime("%d/%m/%Y %H:%M"),
                            record.folder_name,
                            record.document_management_id,
                            record.record.name,
                            move_file,
                        ]
                    )
                    shutil.move(record.record.path, move_file)
                    # FieldFile.delete(save=True)
                    # Deletes the file associated with this instance and
                    # clears all attributes on the field.
                    # https://docs.djangoproject.com/en/4.2/ref/models/fields/#django.db.models.fields.files.FieldFile.delete
                    record.record.delete()
                    count = count + 1
        self.stdout.write(f"Audit file:\n'{csv_file_name}'")
        self.stdout.write(
            f"Complete - Deleted {count} of {qs.count()} matching records..."
        )
