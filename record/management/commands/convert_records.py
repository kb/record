# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from record.service import convert_records


class Command(BaseCommand):
    help = "Process Records"

    def handle(self, *args, **options):
        self.stdout.write("Processing records...")
        count = convert_records()
        self.stdout.write("Complete: {} records converted".format(count))
