# -*- encoding: utf-8 -*-
import dramatiq
import logging

from django.conf import settings

from .service import convert_records


logger = logging.getLogger(__name__)


@dramatiq.actor(queue_name=settings.DRAMATIQ_QUEUE_NAME)
def record_convert(record_pk):
    logger.info("record_convert({})".format(record_pk))
    result = convert_records(record_pk)
    logger.info("record_convert: return {}".format(result))
    return result


@dramatiq.actor(queue_name=settings.DRAMATIQ_QUEUE_NAME)
def record_convert_all():
    logger.info("record_convert_all")
    result = convert_records()
    logger.info("record_convert_all: return {}".format(result))
    return result
