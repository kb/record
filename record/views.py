# -*- encoding: utf-8 -*-
import os

from braces.views import (
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    SuperuserRequiredMixin,
)
from django.contrib.admin.views.decorators import staff_member_required
from django.views.generic import ListView, View
from django_sendfile import sendfile

from base.view_utils import BaseMixin
from .models import Conversion, Record, RecordError


class RecordDetailView(LoginRequiredMixin, BaseMixin, View):
    def get(self, request, *args, **kwargs):
        pk = kwargs.get("pk")
        record = Record.objects.get(pk=pk)
        return sendfile(self.request, str(record.record.file))


class ConversionDetailView(
    LoginRequiredMixin, SuperuserRequiredMixin, BaseMixin, View
):
    def get(self, request, *args, **kwargs):
        pk = kwargs.get("pk")
        conversion = Conversion.objects.get(pk=pk)
        file_path = conversion.get_file_path()
        return sendfile(self.request, file_path)


class RecordConversionDownloadMixin:
    """Download a converted version of a record.

    06/06/2020, Renamed from ``RecordMixin`` to ``RecordConversionMixin``.

    """

    def get_record(self, content_object, **kwargs):
        alias = kwargs.get("alias")
        file_type = kwargs.get("file_type")
        record = Record.objects.for_content_object(content_object)
        if not file_type in Record.FILE_TYPES:
            raise RecordError("'{}' is an invalid file type".format(file_type))
        conversion = Conversion.objects.get(record=record, file_type=file_type)
        return conversion.get_file_path(alias)


class RecordDownloadMixin:
    """Download a record.

    06/06/2020, The original ``RecordMixin`` has been renamed to
    ``RecordConversionDownloadMixin`` (see above).

    """

    def get_record(self, record):
        return os.path.join(
            record.record.field.storage.base_location,
            record.record.file.name,
        )


class RecordListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    model = Record
    paginate_by = 20
