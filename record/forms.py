# -*- encoding: utf-8 -*-
from django import forms

from base.form_utils import FileDropInput
from .models import Record


class RecordUploadForm(forms.ModelForm):
    class Meta:
        model = Record
        fields = ("record",)
        widgets = {"record": FileDropInput}
