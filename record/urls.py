# -*- encoding: utf-8 -*-
from django.urls import re_path

from .views import ConversionDetailView, RecordDetailView, RecordListView


urlpatterns = [
    re_path(
        r"^conversion/(?P<pk>\d+)/$",
        view=ConversionDetailView.as_view(),
        name="record.conversion",
    ),
    re_path(
        r"^view/(?P<pk>\d+)/$",
        view=RecordDetailView.as_view(),
        name="record.view",
    ),
    re_path(r"^$", view=RecordListView.as_view(), name="record.list"),
]
