Records Management
******************

https://www.kbsoftware.co.uk/docs/app-record.html

A *record* is (something) in written, photographic, or other form.

This app will process *records* in the background e.g. convert a Word document
into a PDF file (so it can be displayed in a browser).

Install
=======

Virtual Environment
-------------------

::

  virtualenv --python=python3 venv-record
  # or
  python3 -m venv venv-record
  source venv-record/bin/activate

  pip install -r requirements/local.txt

Testing
=======

::

  find . -name '*.pyc' -delete
  py.test -x

Release
=======

https://www.kbsoftware.co.uk/docs/
